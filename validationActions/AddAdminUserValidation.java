package validationActions;

import java.util.HashMap;

import ownerActions.AddAdminUser;

public class AddAdminUserValidation {
	
	
	public HashMap<String, String> addminuserValditaion(AddAdminUser addAdminUser){
		
		HashMap<String, String> validation = new HashMap<String, String>();
		
		if (mailValidation(addAdminUser.getMail())) {
			
			if (phonenovalidation(addAdminUser.getPhone_no())) {
				
				if (NameValidation(addAdminUser.getName())) {
					
					if (travelsValidation(addAdminUser.getTravels_name())) {
						validation.put("status", "SUCCESS");
					}else{
						validation.put("status", "TRAVELSNAME");
					}
				}else{
					validation.put("status", "NAME");
				}
			}else{
				validation.put("status", "PHONENO");
			}
		}else{
			validation.put("status", "MAIL");
		}
		
		return validation;
	}
	
	
	public boolean mailValidation(String toplace) {

		if (toplace.matches("^[!#$%&'*\\/=?^_+-`{|}~a-zA-Z0-9]{6,30}[@](gmail)[.]com$")) {
			return true;
		} else {
			return false;
		}

	}
	
	public boolean phonenovalidation(String phone) {

		if (phone.matches("[0-9]+")) {
			if (phone.length()<=15) {
				return true;
			}else{
				return false;
			}
		} else {
			return false;
		}

	}
	
	public boolean NameValidation(String bus_name) {

		if (bus_name.matches("[a-zA-Z]+")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean travelsValidation(String fromplace) {

		if (fromplace.matches("[a-zA-Z]+")) {
			return true;
		} else {
			return false;
		}

	}

}
