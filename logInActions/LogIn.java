package logInActions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.mysql.cj.xdevapi.PreparableStatement;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import utility.ConstantValues;
import utility.ConstantValues.USERROLES;
import utility.DatabaseConnection;
import utility.PasswordEncryption;

public class LogIn extends ActionSupport implements SessionAware{

	private Map<String, Object> sessionMap;
	private String userName;
	private String password;

	public String login() {
		
		String returnvalue;
		String ADMIN;
		String PASSANGER;
		
		returnvalue  = logincheck(userName, password);
		
	    ADMIN      =  USERROLES.ADMIN.toString();
	    PASSANGER  =  USERROLES.PASSANGER.toString();
		
	    if (returnvalue!=null) {
	    	
	    	if (returnvalue.equals(ADMIN) || returnvalue.equals(PASSANGER)) {
				
				sessionMap.put("userName", userName);
				
				System.out.println("login "+userName);
				
				return returnvalue;
				
			}else{
				
				return "WRONG";
			}
		}else{
			return "WRONG";
		}
	    
	
	}
	
	
	public String logincheck(String userName ,String password){
		
		PreparedStatement  LoginCheck;
		ResultSet resultForLogInCheck;
		Connection conn;
		String encryptedPassword   = null; 
		String role = null;
		
		encryptedPassword = PasswordEncryption.encryption(password);
		
		try {
			conn = DatabaseConnection.getConnection();
			
			LoginCheck  = conn.prepareStatement(ConstantValues.LOG_INCHECK);
			LoginCheck.setString(1, userName);
			LoginCheck.setString(2, encryptedPassword);
			resultForLogInCheck =LoginCheck.executeQuery();
			
			while(resultForLogInCheck.next()){
				
				role = resultForLogInCheck.getString("role");
				System.out.println(role);
				
			}
			
			return role;
					
		} catch (SQLException e) {
			
			System.out.println("Error code - 1000 "+e.getMessage());
		}
		
		return "input";
	}
	
	public String logout() {
		
		String logoutUserName =null;
		
		 Map<String, Object> session = ActionContext.getContext().getSession();
		 logoutUserName = (String) session.get("userName");
		 
		 System.out.println("Logout "+logoutUserName);
		
		if (sessionMap.containsKey("userName")) {
			
			sessionMap.remove("userName");
		}
		return "LOGOUT";
	}

	
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
