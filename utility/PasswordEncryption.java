package utility;

import java.util.Base64;

public class PasswordEncryption {

	public static String encryption(String input) {
        String BasicBase64format;
        String s;
        int j,k,l,m;
        int ascii;
        int value,conversionvalue;
        int values,aroundvalue,conversionvalues;
        float f;
        char c;
        int extra=46;
        int randnumber=33;


        BasicBase64format= Base64.getEncoder().encodeToString(input.getBytes());
        s =BasicBase64format;
        int a[]=new int[s.length()];
        char[] gfg = s.toCharArray();
        char [] list =s.toCharArray();
        char [] list2=s.toCharArray();
        for ( j = 0; j < gfg.length; j++) {
            c = gfg[j];
            ascii = c;
            a[j]=ascii;
        }

        for( k=0;k<a.length;k++){
            if(a[k]%2==0){
                value=a[k];
                conversionvalue=((value/2)+18*randnumber)/10;

                list[k]=(char)conversionvalue;
                for(l=0;l<list.length;l++){
                    int ad=(25+l*45)/10;
                    int add=extra+ad;
                    list2[l]=(char)add;
                }

            }else if(a[k]%2==1){
                values=a[k];
                f=(values/2)+1;
                aroundvalue=(int)f;
                conversionvalues=((aroundvalue+18)*randnumber)/10;
                list[k]=(char)conversionvalues;
                for(m=0;m<list.length;m++){
                    int ad=(25+m*45)/10;
                    int add=extra+ad;
                    list2[m]=(char)add;
                }
            }
        }
        char array[]=new  char[list.length + list2.length];
        int p1=0;
        int p2=0;
        for(int p=0;p< array.length;p++){
            if(p%2==0){
                if(p1<list.length){
                    array[p]=list[p1];
                    p1=p1+1;
                }
            }else if(p%2==1){
                if(p2<list2.length){
                    array[p]=list2[p2];
                    p2=p2+1;
                }
            }
        }

        return new String(array);
    }
	
	public static String generateAccountPassword() {
		String time = String.valueOf(System.currentTimeMillis()).substring(5,13);
		String account_password = time;
        return account_password;
	}
}
