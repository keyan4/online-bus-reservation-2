package utility;

public class ConstantValues {
	
	public enum USERROLES { ADMIN, PASSANGER}  
	
	public static final String LOG_INCHECK                     = "select * from userrole where username =? and password =?";
	
	public static final String FETCH_MAX_USERROLE_ID           = "SELECT max(account_id) as maxId FROM userrole";
	
	public static final String INSERT_INTO_USERROLE            = "insert into userrole (username,password,role)values(?,?,?)";
	
	public static final String INSERT_INTO_ADMINPERSONAL       = "insert into adminpersonal_details(account_id,name,mail,phone_no,travels_name,table1,table2)values(?,?,?,?,?,?,?)"; 
	
	public static final String INSERT_INTO_PASSANGERPERSONAL   = "insert into passangerpersonal_details(account_id,name,mail,phone_no,table1,table2)values(?,?,?,?,?,?)"; 
	
	public static final String USERNAME_AVAILABLE_CHECK        = "select * from userrole where username =?";
	
	public static final String ROLE_ADMIN                      = "ADMIN";
	
	public static final String ROLE_PASSANGER                  = "PASSANGER";
	
	public static final String ADMIN_SINGLE_TABLE              = "adminS";
	
	public static final String ADMIN_MULTIPLE_TABLE            = "adminM";
	
	public static final String PASSANGER_TICKET_TABLE          = "passangerT";
	
	public static final String PASSANGER_BANK_TABLE            = "passangerB";
}
