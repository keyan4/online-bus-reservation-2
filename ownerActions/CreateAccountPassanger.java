package ownerActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;
import utility.PasswordEncryption;
import utility.SendMail;
import validationActions.AddPassangerValidation;

public class CreateAccountPassanger {
	
	String name;
	String mail;
	String phone_no;
	
	
	public void execute(){
		
		String status;
		boolean userNameAvailable = false;
		HashMap<String, String> validation = new HashMap<String, String>();
		CreateAccountPassanger createAccountPassanger = new CreateAccountPassanger();
		AddPassangerValidation addPassangerValidation = new AddPassangerValidation();
		JSONObject json = new JSONObject();
		
		
		createAccountPassanger.setName(name);
		createAccountPassanger.setMail(mail);
		createAccountPassanger.setPhone_no(phone_no);
		
		validation = addPassangerValidation.passangerValditaion(createAccountPassanger);
		
		status = validation.get("status");
		
		if (status.equals("SUCCESS")) {
			userNameAvailable = UserNameAvailable(createAccountPassanger.getMail());
			if (userNameAvailable) {
			 	if (createpassangerAccount(createAccountPassanger)) {
			 		json.put("status", "SUCCESS");
				}else{
					json.put("status", "FAILURE");
				}
			}else{
				json.put("status", "userNameAvailable");
			}
		 }else{
			 json.put("status", status);
		 }	
		
		 HttpServletResponse response = ServletActionContext.getResponse();
	        response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        PrintWriter writer;
			try {
				 writer = response.getWriter();
				 writer.write(json.toString());
		         writer.flush();
		         writer.close();
				
			} catch (IOException e) {
				System.out.println("Error while handling Add bus Process : "+e.getMessage());
			}
		
	}
	
	public boolean createpassangerAccount(CreateAccountPassanger createAccountPassanger){
		
		PreparedStatement InsertIntoPassangerPersonalDetails;
		PreparedStatement CreateTicketTableForpassanger;
		PreparedStatement CreateBankTableForpassanger;
		PreparedStatement FetchMaxAccountIdUserRole;
		PreparedStatement InsertIntoUserRole;
		ResultSet FetchUserRoleResult;
		Connection conn;
		String passanger_banktablename = null;
		String passanger_tablename     = null;
		String generation_Password     = null;
		String account_password        = null;
		int passanger_id           = 0;
		int maxpassanger_id        = 0;
		boolean sendmail           = false;
		
		
		try{

		    conn = DatabaseConnection.getConnection();
			
			generation_Password = PasswordEncryption.generateAccountPassword();
			account_password =PasswordEncryption.encryption(generation_Password);
			
			sendmail = SendMail.sendingmail(account_password, createAccountPassanger.getMail(), createAccountPassanger.getName());
				
			if (sendmail) {
				
				 FetchMaxAccountIdUserRole = conn.prepareStatement(ConstantValues.FETCH_MAX_USERROLE_ID );
			     FetchUserRoleResult = FetchMaxAccountIdUserRole.executeQuery();

			        while (FetchUserRoleResult.next()) {

			        	passanger_id = FetchUserRoleResult.getInt("maxstudvalue");

			        }
			        
			    if (passanger_id==0) {
			    	maxpassanger_id = passanger_id+1010;
			    }else{
			    	maxpassanger_id = passanger_id+1;
			    }
			    
			    passanger_tablename      = ConstantValues.PASSANGER_TICKET_TABLE+maxpassanger_id;
			    passanger_banktablename  = ConstantValues.PASSANGER_BANK_TABLE+maxpassanger_id;
			    
			    InsertIntoUserRole = conn.prepareStatement(ConstantValues.INSERT_INTO_USERROLE);
			    InsertIntoUserRole.setString(1,createAccountPassanger.getName());
			    InsertIntoUserRole.setString(2,account_password);
			    InsertIntoUserRole.setString(3,ConstantValues.ROLE_PASSANGER);
		        InsertIntoUserRole.executeUpdate();
				
		        InsertIntoPassangerPersonalDetails = conn.prepareStatement(ConstantValues.INSERT_INTO_PASSANGERPERSONAL);
		        InsertIntoPassangerPersonalDetails.setInt(1,maxpassanger_id);
		        InsertIntoPassangerPersonalDetails.setString(2,createAccountPassanger.getName());
		        InsertIntoPassangerPersonalDetails.setString(3,createAccountPassanger.getMail());
		        InsertIntoPassangerPersonalDetails.setString(4,createAccountPassanger.getPhone_no());
		        InsertIntoPassangerPersonalDetails.setString(5,passanger_tablename);
		        InsertIntoPassangerPersonalDetails.setString(6,passanger_banktablename);
		        InsertIntoPassangerPersonalDetails.executeUpdate();
		        
		        
		        CreateTicketTableForpassanger =conn.prepareStatement("create table "+passanger_tablename+" (ticket_id int auto_increment primary key,bus_name varchar(100),bus_number varchar(100),from_place varchar(100),to_place varchar(100),date varchar(100),arrival_time varchar(50),seat_type_id1 varchar(50),seat_id1 varchar(50),rate varchar(50))");
		        CreateTicketTableForpassanger.executeUpdate();
		        
		        CreateBankTableForpassanger = conn.prepareStatement("create table "+passanger_banktablename+" (card_id int auto_increment primary key,card_number varchar(150),ccv varchar(50))");
		        CreateBankTableForpassanger.executeUpdate();
		        
		        return true;
			}else{
				System.out.println("Error code - 1003 ");
				
				return false;
			}
	        
		}catch (Exception e) {
			
			System.out.println("Error code -1004 "+e.getMessage());
			
			return false;
		}
		
		
		
	}
	
	
	public boolean UserNameAvailable(String username){
		
		PreparedStatement UserNameAvailableCheck;
		ResultSet UserNameAvailableResult;
		Connection conn;
		boolean returnValue;
		
		try {
			
			conn = DatabaseConnection.getConnection();
			
			UserNameAvailableCheck =conn.prepareStatement(ConstantValues.USERNAME_AVAILABLE_CHECK);
			UserNameAvailableCheck.setString(1, username);
			UserNameAvailableResult = UserNameAvailableCheck.executeQuery();
			
			returnValue = UserNameAvailableResult.next();
			
			if (returnValue) {
				return false;
			}else{
				return true;
			}
			
		} catch (Exception e) {
			System.out.println("Error code - 1002 "+e.getMessage());
			
			return true;
		}
	}
	
	
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	
	

}
