package ownerActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import dtoFiles.AddSingleBusDto;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;
import utility.PasswordEncryption;
import utility.SendMail;
import validationActions.AddAdminUserValidation;

public class AddAdminUser{
	
	String name;
	String mail;
	String travels_name;
	String phone_no;
	
	
	public void execute(){
		
		String status;
		boolean userNameAvailable = false;
		AddAdminUserValidation addAdminUserValidation = new AddAdminUserValidation();
		HashMap<String, String> validation = new HashMap<String, String>();
		JSONObject json = new JSONObject();
		AddAdminUser addAdminUser = new AddAdminUser();
		addAdminUser.setName(name);
		addAdminUser.setMail(mail);
		addAdminUser.setPhone_no(phone_no);
		addAdminUser.setTravels_name(travels_name);
		
		validation  = addAdminUserValidation.addminuserValditaion(addAdminUser);
		
		status = validation.get("status");
		
		if (status.equals("SUCCESS")) {
			userNameAvailable = UserNameAvailable(addAdminUser.getMail());
			if (userNameAvailable) {
			 	if (addadminuser(addAdminUser)) {
			 		json.put("status", "SUCCESS");
				}else{
					json.put("status", "FAILURE");
				}
			}else{
				json.put("status", "userNameAvailable");
			}
		 }else{
			 json.put("status", status);
		 }	
		
		HttpServletResponse response = ServletActionContext.getResponse();
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    PrintWriter writer;
	    try {
	    	 writer = response.getWriter();
			 writer.write(json.toString());
	         writer.flush();
	         writer.close();
	        } catch (IOException e) {
				System.out.println("Error while handling Add bus Process : "+e.getMessage());
			}
			
		}
		
		
		public boolean addadminuser(AddAdminUser addAdminUser){
			
			
			PreparedStatement FetchMaxAccountIdUserRole;
			PreparedStatement InsertIntoUserRole;
			PreparedStatement InsertIntoAdminPersonalDetails;
			PreparedStatement CreatesingleTableForadmin;
		    PreparedStatement AutoIncrementsingleAdminBusTable;
		    PreparedStatement CreatemultipleTableForadmin;
		    PreparedStatement AutoIncrementsmultipleAdminBusTable;
			ResultSet FetchUserRoleResult;
			Connection conn;
			String generation_Password  = null;
			String account_password     = null;
			String adminTablesingle     = null;
			String adminTablemultiple   = null;
			int maxUserRoleId  = 0;
			int userRoleId     = 0;
			boolean sendMail = false;
			
			
			try{

				conn = DatabaseConnection.getConnection();
				
				generation_Password = PasswordEncryption.generateAccountPassword();
				account_password =PasswordEncryption.encryption(generation_Password);
				
				sendMail = SendMail.sendingmail(account_password, addAdminUser.getMail(), addAdminUser.getName());
				
				if (sendMail) {
					
					FetchMaxAccountIdUserRole =conn.prepareStatement(ConstantValues.FETCH_MAX_USERROLE_ID);
					FetchUserRoleResult = FetchMaxAccountIdUserRole.executeQuery();

				        while (FetchUserRoleResult.next()) {

				        	userRoleId = FetchUserRoleResult.getInt("maxId");

				        }
				        
				    if (userRoleId == 0) {
				    	maxUserRoleId = userRoleId+1010;
				    }else{
				    	maxUserRoleId = userRoleId+1;
				    }
				  
					adminTablesingle   = ConstantValues.ADMIN_SINGLE_TABLE+maxUserRoleId;
					adminTablemultiple = ConstantValues.ADMIN_MULTIPLE_TABLE+maxUserRoleId;
					
					InsertIntoUserRole = conn.prepareStatement(ConstantValues.INSERT_INTO_USERROLE);
			        InsertIntoUserRole.setString(1,addAdminUser.getMail());
			        InsertIntoUserRole.setString(2,account_password);
			        InsertIntoUserRole.setString(3,ConstantValues.ROLE_ADMIN);
			        InsertIntoUserRole.executeUpdate();
			        
			        InsertIntoAdminPersonalDetails = conn.prepareStatement(ConstantValues.INSERT_INTO_ADMINPERSONAL);
			        InsertIntoAdminPersonalDetails.setInt(1,maxUserRoleId);
			        InsertIntoAdminPersonalDetails.setString(2,addAdminUser.getName());
			        InsertIntoAdminPersonalDetails.setString(3,addAdminUser.getMail());
			        InsertIntoAdminPersonalDetails.setString(4,addAdminUser.getPhone_no());
			        InsertIntoAdminPersonalDetails.setString(5,addAdminUser.getTravels_name());
			        InsertIntoAdminPersonalDetails.setString(6,adminTablesingle);
			        InsertIntoAdminPersonalDetails.setString(7,adminTablemultiple);
			        InsertIntoAdminPersonalDetails.executeUpdate();
			        
			       
			        CreatesingleTableForadmin = conn.prepareStatement("create table "+adminTablesingle+" (bus_id int auto_increment primary key,bus_name varchar(150),bus_number varchar(50),from_place varchar(100),to_place varchar(100),date varchar(50),arrival_time varchar(50),departure_time varchar(50),seat_type_id varchar(50),seat_count varchar(50),ac_type_id varchar(50),bus_tablename varchar(100),rate varchar(50),bus_tablename2 varchar(50)) ");
			        CreatesingleTableForadmin.executeUpdate();
					
			        
			        AutoIncrementsingleAdminBusTable = conn.prepareStatement("alter table "+adminTablesingle+" auto_increment = 1000 ");
			        AutoIncrementsingleAdminBusTable.executeUpdate();
			        
			        
			        CreatemultipleTableForadmin = conn.prepareStatement("create table "+adminTablemultiple+" (bus_id int auto_increment primary key,bus_name varchar(150),bus_number varchar(50),from_place varchar(100),to_place varchar(100),date varchar(50),arrival_time varchar(50),departure_time varchar(50),seat_type_id1 varchar(50),seat_type_id2 varchar(50),seat_count1 varchar(50),seat_count2 varchar(50),ac_type_id varchar(50),rate1 varchar(50),rate2 varchar(50),bus_tablename varchar(50),bus_tablename2 varchar(50)) ");
			        CreatemultipleTableForadmin.executeUpdate();
					
			     
			        AutoIncrementsmultipleAdminBusTable = conn.prepareStatement("alter table "+adminTablemultiple+" auto_increment = 1000 ");
			        AutoIncrementsmultipleAdminBusTable.executeUpdate();
				
			        return true;
					
				}else{
					
					System.out.println("Error code -1000 ");
					return false;
				}
				
		        
		        }catch (Exception e) {
		        	
		        	System.out.println("Error code - 1001 "+e.getMessage());
		        	
		        	return false;
				}
			
		}
		
		public boolean UserNameAvailable(String username){
			
			PreparedStatement UserNameAvailableCheck;
			ResultSet UserNameAvailableResult;
			Connection conn;
			boolean returnValue;
			
			try {
				
				conn = DatabaseConnection.getConnection();
				
				UserNameAvailableCheck =conn.prepareStatement(ConstantValues.USERNAME_AVAILABLE_CHECK);
				UserNameAvailableCheck.setString(1, username);
				UserNameAvailableResult = UserNameAvailableCheck.executeQuery();
				
				returnValue = UserNameAvailableResult.next();
				
				if (returnValue) {
					return false;
				}else{
					return true;
				}
				
			} catch (Exception e) {
				System.out.println("Error code - 1002 "+e.getMessage());
				
				return true;
			}
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		public String getTravels_name() {
			return travels_name;
		}
		public void setTravels_name(String travels_name) {
			this.travels_name = travels_name;
		}
		public String getPhone_no() {
			return phone_no;
		}
		public void setPhone_no(String phone_no) {
			this.phone_no = phone_no;
		}
	
}
